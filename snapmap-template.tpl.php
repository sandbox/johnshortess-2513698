<?php
/**
 * @file
 * SnapMap template file.
 */
?>
<h3><?php print $title; ?></h3>
<div id="snapmap__infobox">
  <h2 class="snapmap__stateName"><?php print $title; ?></h2>
    <?php if(isset($fieldlabels['field1'])): ?>
     <p class="snapmap__field snapmap__field1">
         <span class="snapmap__field1--label"><?php print $fieldlabels['field1']; ?></span><br>
         <span><strong class="snapmap__field1--value"></strong></span>
      </p>
    <?php if(isset($fieldlabels['field2'])): ?>
      <p class="snapmap__field snapmap__field2">
        <span class="snapmap__field2--label"><?php print $fieldlabels['field2']; ?></span><br>
        <span><strong class="snapmap__field2--value"></strong></span>
      </p>
    <?php if(isset($fieldlabels['field3'])): ?>
      <p class="snapmap__field snapmap__field3">
          <span class="snapmap__field3--label"><?php print $fieldlabels['field3']; ?></span><br>
          <span><strong class="snapmap__field3--value"></strong></span>
      </p>
    <?php if(isset($fieldlabels['field4'])): ?>
      <p class="snapmap__field snapmap__field4">
          <span class="snapmap__field4--label"><?php print $fieldlabels['field4']; ?></span><br>
          <span><strong class="snapmap__field4--value"></strong></span>
      </p>
    <?php if(isset($fieldlabels['field5'])): ?>
      <p class="snapmap__field snapmap__field5">
          <span class="snapmap__field5--label"><?php print $fieldlabels['field5']; ?></span><br>
          <span><strong class="snapmap__field5--value"></strong></span>
      </p>
  <p class="debug"></p>
</div>

<div class="snapmap__map" style="display:block;width:480px;height:380px;">
    <svg id="snapmap__map" viewBox="0 0 1000 700" preserveAspectRatio></svg>
</div>
<br clear="both" />
<div id="snapmap__legend">
  <div id="snapmap__legend--label" style="display: block;"><?php print $legend['label']; ?></div>
  <p id="snapmap__legend--description" style="display: block;"><?php print $legend['description']; ?></p>
  <div id="snapmap__legend--minimum"><?php print $legend['minimum']; ?></div>
  <div id="snapmap__legend--gradient"></div>
  <div id="snapmap__legend--maximum"><?php print $legend['maximum']; ?></div>
  <br clear="left" />
</div>
