Better documentation is coming soon! In the meantime, here are some quick
pointers:

1. You need to download the Snap.SVG library from http://snapsvg.io. Unzip the
file and place it in your libraries directory so that snap.svg-min.js is at
sites/*/libraries/snap.svg/snap.svg-min.js.

2. Your data file needs to be in JSON format, with objects whose keys match
the IDs of the states (or other political subdivisions) within your SVG map.
Within each of those objects should be key-value pairs, with keys named field1,
field2, etc. (Look for an update soon that will eliminate the need to rename
fields in your data file.) Up to 5 data fields are supported at this time. The
human-readable name of the state (or other political subdivision) should be
included in a key called "stateName." If you include a URL in a key called
"link," the user will be taken to that URL when clicking/tapping the state.

Here's a sample of what the data should look like:

"AR": {
    "stateName":"Arkansas",
    "link": "/test/AR",
    "field1":10,
    "field2":7,
    "field3":23,
    "field4":12,
    "field5":33
  },

3. The opacity settings are a little confusing. "Opacity Field" is the field in
your JSON (field1-field5) that should be used to control opacity. "Minimum
Value" is the value within your dataset that should be represented as 0%
opacity on the map, and "Maximum Value" is the value that should be represented
as 100% opacity on the map. Look for lots more flexibility in how data is
visually represented on the map in future updates.
