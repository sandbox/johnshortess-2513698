/**
 * JavaScript for SnapMap.
 *
 * @file: snapmap.js
 */

jQuery(document).ready(function ($) {
    // Load map data.
    var json_path = Drupal.settings.basePath + Drupal.settings.snapmap_json_path;
    var map_path = Drupal.settings.basePath + Drupal.settings.snapmap_map_path;
    var map_data;
    $.getJSON(json_path).done(function(data) {
        map_data = data;
    });

    // Update the info box with data for the given state.
    // @ToDo: Hide the infobox if there's no 'default' array in the JSON file, then only show it when a state is hovered over.
    function updateInfobox(key) {
        var data = map_data[key];
        infobox = $('#snapmap__infobox');
        // if ((key == 'default') && !('default' in data)) {
        //    $(infobox).hide();
        // }
        $('.snapmap__stateName', infobox).text(data.stateName);
        $('.snapmap__field1--value', infobox).text(data.field1);
        $('.snapmap__field2--value', infobox).text(data.field2);
        $('.snapmap__field3--value', infobox).text(data.field3);
        $('.snapmap__field4--value', infobox).text(data.field4);
        $('.snapmap__field5--value', infobox).text(data.field5);
    }

    function clickHandler(stateID) {
        var data = map_data[stateID];
        if (data.link) {
            window.location = data['link'];
        }
    }

    // Convert hex triplet to RGB.
    function hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    // Create the map canvas.
    var s = Snap("#snapmap__map");
    var g = s.g();
    var stateFragments = {};

    // Load the base map.
    var image = Snap.load(map_path, function (f) {
        // Grab each state from the SVG so we can manipulate it.
        for (var state in map_data) {
            stateFragments[state] = f.select("#" + state);
        }
        s.append(f);
        // Show default data in the infobox.
        updateInfobox('default');

        // Set opacity for each map area.
        for (var state in stateFragments) {
            if (stateFragments[state]) {
                data = map_data[state];
                opacityField = Drupal.settings.snapmap_opacity_field;
                opacityMinimum = Drupal.settings.snapmap_opacity_minimum;
                opacityMaximum = Drupal.settings.snapmap_opacity_maximum;
                opacityColor = Drupal.settings.snapmap_opacity_color;

                var opacity = ((data[opacityField] - opacityMinimum) / (opacityMaximum - opacityMinimum));
                var color = hexToRgb(opacityColor);
                $('#snapmap__legend--gradient').css("background-image", "linear-gradient(left, rgba(" + color['r'] + ", " + color['g'] + ", " + color['b'] + ", 0), rgba(" + color['r'] + ", " + color['g'] + ", " + color['b'] + ", 1) )");
                $('#snapmap__legend--gradient').css("background-image", "-moz-linear-gradient(left, rgba(" + color['r'] + ", " + color['g'] + ", " + color['b'] + ", 0), rgba(" + color['r'] + ", " + color['g'] + ", " + color['b'] + ", 1) )");
                $('#snapmap__legend--gradient').css("background-image", "-webkit-linear-gradient(left, rgba(" + color['r'] + ", " + color['g'] + ", " + color['b'] + ", 0), rgba(" + color['r'] + ", " + color['g'] + ", " + color['b'] + ", 1) )");

                stateFragments[state].attr({"fill-opacity": opacity});
                // Click handler - go to link for selected map area.
                stateFragments[state].click(function () {
                    var stateID = this.attr("id");
                    clickHandler(stateID);
                });

                // Mouseover handler - change the info box to the selected map area.
                stateFragments[state].mouseover(function () {
                    var stateID = this.attr("id");
                    updateInfobox(stateID);
                });
                // Mouseout handler - change infobox back to default.
                stateFragments[state].mouseout(function () {
                    updateInfobox('default');
                });
            }
        }
    });

});
